


session = Capybara::Session.new(:selenium);
names, namesNoWhitespaces, emails, total_accounts = nil;
Given(/^I register emails$/) do
  total_accounts = 2;
  names = generateNames(session, total_accounts);
  namesNoWhitespaces = names.map { |x| x.gsub(/\s+/, "")[0,15] }
  emails = generateEmails(namesNoWhitespaces);

  end

Then(/^I register accounts$/) do
  registerAccounts(session, namesNoWhitespaces, emails);
end




  def generateNames(session, total_accounts)
    session.visit("http://random-name-generator.info/random/?n=#{total_accounts}&g=1&st=3");
    names = session.all(".nameList li").map { |a| a.text };
    puts(names);
    return names;
  end

  def generateEmails(names)
    emails = names.map { |x| (x + "@yopmail.com").downcase }
    puts(emails);
    return emails;
  end

  def registerAccounts(session, names, emails)
    accounts = Array.new;
    password = "rubycapybara";
    i = 0;
    while i < emails.length
      session.visit("https://dev.by/registration");
      session.fill_in id: "user_username", with: names[i]
      session.fill_in id: "user_email", with: emails[i]
      session.fill_in id: "user_password", with: password
      session.fill_in id: "user_password_confirmation", with: password
      session.find("#user_agreement").set(true)
      session.find(".submit").click
      puts("email #{emails[i]} successfully registered")
      confirmAccount(emails[i]);
      accounts.push({name: names[i], email: emails[i], password: password})
      i=i+1;
    end
    accounts.each { |a| puts a }
  end

  def confirmAccount(email)
    url = "http://www.yopmail.com/ru/inbox.php?login=#{email}&p=1&d=&ctrl=&scrl=&spam=true&yf=005&&p=r&d=&ctrl=&scrl=&spam=true&yf=115&yp=CAQH4ZGDkAmZjBGZjZQplBN&yj=WBGx5Zmx3ZwxlBGx5AQR4ZN&v=2.6&r_c=&id=";
    session = Capybara::Session.new(:selenium);
    session.visit(url);
    session.find("span.lmf", text: "Dev.by").click;
    session.switch_to_window(session.windows.last());
    session.find("#mailmillieu a").click;
    sleep(3)
    puts("email #{email} successfully confirmed")
  end
