
When(/^I visit google.com$/) do
  visit('http://google.com')
end
Then(/^I enter value$/) do
  value = SecureRandom.hex(33);
  fill_in 'q', with: value;
  input = page.find("input[name='q']");
  input.send_keys(:enter);
  sleep(30);
end

Then(/^I should see result$/) do
  have_css('div#res li');
  sleep(30);
end