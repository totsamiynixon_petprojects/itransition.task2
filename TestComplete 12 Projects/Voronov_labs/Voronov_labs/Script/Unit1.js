﻿function choose_eng_view(context){
  context.UIAObject("Menu").Click();
  context.UIAObject("PaneRoot").UIAObject("Mode_DropDown").UIAObject("ListViewHeaderItem").UIAObject("Programmer_Calculator").Click();
}


function check_buttons_availability(context){
  if(context.UIAObject("LandmarkTarget", 4).UIAObject("Three").Enabled) {
    Log.Message("Three button is enabled.");
  }
  if(context.UIAObject("LandmarkTarget", 4).UIAObject("Four").Enabled){
    Log.Message("Four button is enabled.");
  }
  if(context.UIAObject("LandmarkTarget", 4).UIAObject("Five").Enabled){
    Log.Message("Five button is enabled.");
  }
  if(context.UIAObject("LandmarkTarget").UIAObject("Or")){
     Log.Message("Or button is enabled.");
  }
}

function get_window_pos(context){
  Log.Message("Position of window: left -  " + context.ScreenLeft + ", top - " + context.ScreenTop);
}

function set_window_pos(context){
   context.Position(context.ScreenLeft,context.ScreenTop,context.Width,context.Height);
   Log.Message("Position of window: left -  " + context.ScreenLeft + ", top - " + context.ScreenTop);
}
function clear_input_value(context){
  context.UIAObject("LandmarkTarget", 3).UIAObject("Clear").Click();
  Log.Message("Clear input");
}

function test_calc() {
  var context = Sys.Process("Microsoft.WindowsCalculator").UIAObject("Calculator");
  choose_eng_view(context);
  check_buttons_availability(context);
  get_window_pos(context);
  set_window_pos(context);
  clear_input_value(context);
}